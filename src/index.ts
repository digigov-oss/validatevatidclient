import soapClient from './soapClient.js';
import config from './config.json'; 

export type checkVatResponse = {
    countryCode:string;
    vatNumber:string;
    requestDate:Date;
    valid:boolean;
    name:string;
    address:string;
}

/**
 * @type Overrides
 * @description Overrides for the SOAP client
 * @param {boolean} prod - Set to true for production environment
 * @param {string} endpoint - Override the endpoint
 */
export type Overrides = {
   endpoint?:string;
   prod?:boolean;
}

/**
 * 
 * @param vatNumber string;
 * @param countryCode string;
 * @param overrides Overrides;
 * @returns checkVatResponse
 */
export const getVatInfo = async (countryCode:string, vatNumber:string, overrides?:Overrides) => {
    const prod = overrides?.prod ?? false;
    const endpoint = overrides?.endpoint ?? "";
    const wsdl = prod==true? config.prod.wsdl : config.test.wsdl;
    try {
        const s = new soapClient(wsdl,endpoint);
        const vatInfo = await s.getVatInfo(countryCode,vatNumber);
        return vatInfo;
        } catch (error) {
           throw(error);
          }
}
export default getVatInfo;