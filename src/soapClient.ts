import thesoap from 'soap';
let soap:any = thesoap;
try {
   soap = require('soap');
} catch (error) {
  //my hackish way to make soap work on both esm and cjs
  //theshoap on esm is undefined
  //On esm require is not defined, however on cjs require can be used.
  //So we try to use require and if it fails we use the thesoap module
}

/**
 * SOAP client for VIES VAT validation
 * @class Soap
 * @description SOAP client for validating VAT numbers
 * @param {string} wsdl - The URL of the SOAP service
 * @param {string} endpoint
 */
class Soap {
  private _wsdl: string;
  private _endpoint: string;

  constructor(wsdl: string,endpoint: string) {
    this._wsdl = wsdl;
    this._endpoint = endpoint;
  }

  public async init() {
    try {
      const client = await soap.createClientAsync(this._wsdl);
      return client;
    } catch (e) {
      throw e;
    }
  }

  public async getVatInfo(countryCode:string,vatNumber:string,) {
    try {
      const client = await this.init();
      const args = {
            countryCode: countryCode,
            vatNumber: vatNumber
      }
      if (this._endpoint) {
        client.setEndpoint(this._endpoint);
      }
      const result = await client.checkVatAsync(args);
      // fix the date returned from the soap service, the service returns date like a string 2022-11-22+01:00
      // but the date object expects a string like 2022-11-22T01:00:00.000Z
      // on headers the date is returned correctly
      return {...result[0],requestDate:new Date(client.lastResponseHeaders.date)};
    } catch (e) {
      throw e;
    }
  }
}

export default Soap;