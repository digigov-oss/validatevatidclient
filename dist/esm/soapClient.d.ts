/**
 * SOAP client for VIES VAT validation
 * @class Soap
 * @description SOAP client for validating VAT numbers
 * @param {string} wsdl - The URL of the SOAP service
 * @param {string} endpoint
 */
declare class Soap {
    private _wsdl;
    private _endpoint;
    constructor(wsdl: string, endpoint: string);
    init(): Promise<any>;
    getVatInfo(countryCode: string, vatNumber: string): Promise<any>;
}
export default Soap;
