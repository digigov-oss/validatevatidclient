import soapClient from './soapClient.js';
import config from './config.json';
/**
 *
 * @param vatNumber string;
 * @param countryCode string;
 * @param overrides Overrides;
 * @returns checkVatResponse
 */
export const getVatInfo = async (countryCode, vatNumber, overrides) => {
    const prod = overrides?.prod ?? false;
    const endpoint = overrides?.endpoint ?? "";
    const wsdl = prod == true ? config.prod.wsdl : config.test.wsdl;
    try {
        const s = new soapClient(wsdl, endpoint);
        const vatInfo = await s.getVatInfo(countryCode, vatNumber);
        return vatInfo;
    }
    catch (error) {
        throw (error);
    }
};
export default getVatInfo;
