export declare type checkVatResponse = {
    countryCode: string;
    vatNumber: string;
    requestDate: Date;
    valid: boolean;
    name: string;
    address: string;
};
/**
 * @type Overrides
 * @description Overrides for the SOAP client
 * @param {boolean} prod - Set to true for production environment
 * @param {string} endpoint - Override the endpoint
 */
export declare type Overrides = {
    endpoint?: string;
    prod?: boolean;
};
/**
 *
 * @param vatNumber string;
 * @param countryCode string;
 * @param overrides Overrides;
 * @returns checkVatResponse
 */
export declare const getVatInfo: (countryCode: string, vatNumber: string, overrides?: Overrides | undefined) => Promise<any>;
export default getVatInfo;
