"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const soap_1 = __importDefault(require("soap"));
let soap = soap_1.default;
try {
    soap = require('soap');
}
catch (error) {
    //my hackish way to make soap work on both esm and cjs
    //theshoap on esm is undefined
    //On esm require is not defined, however on cjs require can be used.
    //So we try to use require and if it fails we use the thesoap module
}
/**
 * SOAP client for VIES VAT validation
 * @class Soap
 * @description SOAP client for validating VAT numbers
 * @param {string} wsdl - The URL of the SOAP service
 * @param {string} endpoint
 */
class Soap {
    constructor(wsdl, endpoint) {
        this._wsdl = wsdl;
        this._endpoint = endpoint;
    }
    init() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const client = yield soap.createClientAsync(this._wsdl);
                return client;
            }
            catch (e) {
                throw e;
            }
        });
    }
    getVatInfo(countryCode, vatNumber) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const client = yield this.init();
                const args = {
                    countryCode: countryCode,
                    vatNumber: vatNumber
                };
                if (this._endpoint) {
                    client.setEndpoint(this._endpoint);
                }
                const result = yield client.checkVatAsync(args);
                // fix the date returned from the soap service, the service returns date like a string 2022-11-22+01:00
                // but the date object expects a string like 2022-11-22T01:00:00.000Z
                // on headers the date is returned correctly
                return Object.assign(Object.assign({}, result[0]), { requestDate: new Date(client.lastResponseHeaders.date) });
            }
            catch (e) {
                throw e;
            }
        });
    }
}
exports.default = Soap;
