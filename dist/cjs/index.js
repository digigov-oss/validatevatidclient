"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getVatInfo = void 0;
const soapClient_js_1 = __importDefault(require("./soapClient.js"));
const config_json_1 = __importDefault(require("./config.json"));
/**
 *
 * @param vatNumber string;
 * @param countryCode string;
 * @param overrides Overrides;
 * @returns checkVatResponse
 */
const getVatInfo = (countryCode, vatNumber, overrides) => __awaiter(void 0, void 0, void 0, function* () {
    var _a, _b;
    const prod = (_a = overrides === null || overrides === void 0 ? void 0 : overrides.prod) !== null && _a !== void 0 ? _a : false;
    const endpoint = (_b = overrides === null || overrides === void 0 ? void 0 : overrides.endpoint) !== null && _b !== void 0 ? _b : "";
    const wsdl = prod == true ? config_json_1.default.prod.wsdl : config_json_1.default.test.wsdl;
    try {
        const s = new soapClient_js_1.default(wsdl, endpoint);
        const vatInfo = yield s.getVatInfo(countryCode, vatNumber);
        return vatInfo;
    }
    catch (error) {
        throw (error);
    }
});
exports.getVatInfo = getVatInfo;
exports.default = exports.getVatInfo;
