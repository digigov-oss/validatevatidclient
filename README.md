# validateVatID 

Validate VAT ID's from different countries via VIES (VAT Information Exchange System).

#### * known issues
VIES advertises wrong endpoints(!) for the `checkVat` service on test and on production WSDL, it uses `http` while the server redirects always at `https` protocol. So, you have to use (override) the endpoints.
You can do that by setting the `endpoint` property on the `overrides` object.

i.e. for test use:
```
const overrides = {
    endpoint: 'https://ec.europa.eu/taxation_customs/vies/test-services/checkVatTestService',
}
```
and for production use:
```
    const overrides = {
        prod: true,
        endpoint: 'https://ec.europa.eu/taxation_customs/vies/services/checkVatService'
    }
```