import getData from '../src/index';
import config from './config.json'; 
const test = async () => {
    const overrides = {
        prod: false,
        endpoint: 'https://ec.europa.eu/taxation_customs/vies/test-services/checkVatTestService'
    }

    try {
        const data = await getData(config.countryCode,config.vatNumber,overrides);
        return data;
    } catch (error) {
        console.log(error);
    }
}

test().then((vatinfo) => { console.log('checkVatResponse',vatinfo); });